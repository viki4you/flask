from sqlalchemy import Column, Integer, String, Boolean
from database import Base

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    email = Column(String(120), unique=True)
    password = Column(String(120), unique=False)
    firstname = Column(String(255), unique=False)
    lastname = Column(String(255), unique=False)
    street = Column(String(255), unique=False)
    housenumber = Column(String(255), unique=False)
    addresshint = Column(String(255), unique=False)
    zip = Column(String(255), unique=False)
    city = Column(String(255), unique=False)
    country = Column(String(255), unique=False)
    email_confirmed = Column(Boolean, unique=False, default= False)

    def __init__(self,
                 email=None,
                 password=None,
                 firstname=None,
                 lastname=None,
                 street=None,
                 housenumber=None,
                 addresshint=None,
                 zip=None,
                 city=None,
                 country=None,
                 email_confirmed=False):
        self.email = email
        self.password = password
        self.firstname = firstname
        self.lastname = lastname
        self.street = street
        self.housenumber = housenumber
        self.addresshint = addresshint
        self.zip = zip
        self.city = city
        self.country = country
        self.email_confirmed = email_confirmed

    def __repr__(self):
        return f'<User {self.email!r}>'