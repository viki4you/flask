from flask import Flask
from flask import render_template
from wtforms import Form, BooleanField, StringField, PasswordField, validators
from flask import request, redirect
from flask import url_for
from flask import current_app
from flask_mail import Mail, Message
from itsdangerous import URLSafeTimedSerializer
from sqlalchemy import select, exists

#database
from database import db_session
from models import User

app = Flask(__name__, static_url_path='/static')

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

@app.route("/")
def index():
    return render_template('index.html')

class LoginForm(Form):
    email = StringField('Email Addresse', [
        validators.DataRequired()
    ])
    password = PasswordField('Passwort', [
        validators.DataRequired()
    ])

@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User(form.email.data,
                    form.password.data)
        db_session.add(user)
        flash('Sie sind eingeloggt')
        return redirect(url_for('logged_in'))
    return render_template('login.html', form=form)

class RegistrationForm(Form):
    email = StringField('Email Addresse', [
        validators.DataRequired(),
        validators.Regexp('^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$',
                          message="Keine gültige Email Adresse")
    ])
    password = PasswordField('Passwort', [
        validators.DataRequired(),
        validators.Regexp('^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$',
                          message="Das Passwort muss aus mindestens 8 Zeichen, "
                                  "einem Buchstaben, einer Zahl und einem Sonderzeichen bestehen")
    ])
    firstname = StringField('Vorname', [
        validators.DataRequired()
    ])
    lastname = StringField('Nachname', [
        validators.DataRequired()
    ])
    street = StringField('Straße', [
        validators.DataRequired()
    ])
    housenumber = StringField('Hausnummer', [
        validators.DataRequired()
    ])
    addresshint = StringField('Adressenzusatz')
    zip = StringField('Postleitzahl', [
        validators.DataRequired(),
        validators.Regexp('^[0-9]*$', message="Nur Zahlen erlaubt")
    ])
    city = StringField('Stadt', [
        validators.DataRequired()
    ])
    accept_tos = BooleanField('AGBs bestätigen', [validators.DataRequired()])

@app.route('/confirm_your_email')
def confirm_your_email():
    return render_template('confirm_your_email.html')

app.config['MAIL_SERVER']='smtp.mailtrap.io'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USERNAME'] = 'f8d07572d23aa3'
app.config['MAIL_PASSWORD'] = '256a82ca876636'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = False
mail = Mail(app)

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        email = form.email.data
        user = User(form.email.data,
                    form.password.data,
                    form.firstname.data,
                    form.lastname.data,
                    form.street.data,
                    form.housenumber.data,
                    form.zip.data,
                    form.city.data,
                    'Germany',
                    False)
        db_session.add(user)
        db_session.commit()

        ts = URLSafeTimedSerializer('test')
        token = ts.dumps(email, salt='email-confirm-key')

        confirm_url = url_for(
            'confirm_email',
            token=token,
            _external=True)

        html = render_template(
            'email/activate.html',
            confirm_url=confirm_url)

        send_email(user.email, 'Email Bestätigen', html)
        return redirect(url_for('confirm_your_email'))
    return render_template('register.html', form=form)

def send_email(email, subject, html):
   msg = Message(subject, sender = 'sender@test.de', recipients = [email])
   msg.body = html
   mail.send(msg)

@app.route('/confirm_email/<token>')
def confirm_email(token):
    ts = URLSafeTimedSerializer('test')
    try:
        email = ts.loads(token, salt="email-confirm-key", max_age=86400)
        exists = db_session.query(User).filter_by(email=email).first() is not None
        if exists != None:
            user = User.query.filter_by(email=email).first()
            user.email_confirmed = True
            db_session.commit()
    except:
        return redirect(url_for('email_confimation_failed'))

    return redirect(url_for('signin'))